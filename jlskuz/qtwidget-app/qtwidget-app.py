import info
from Package.CMakePackageBase import CMakePackageBase


class subinfo(info.infoclass):
    def setTargets(self):
        self.svnTargets['master'] = 'https://invent.kde.org/jlskuz/qtwidget-android-app.git'
        self.defaultTarget = 'master'

        self.description = "QtWidget Demo App"
        self.displayName = "QtWidget Demo App"

    def setDependencies(self):
        self.buildDependencies["virtual/base"] = None
        self.buildDependencies["kde/frameworks/extra-cmake-modules"] = None
        self.runtimeDependencies["libs/qt/qtbase"] = None
        self.runtimeDependencies["libs/qt/qtsvg"] = None
        self.runtimeDependencies["libs/qt/qttools"] = None

class Package(CMakePackageBase):
    def __init__(self):
        super().__init__()

    def createPackage(self):
        self.ignoredPackages.append("binary/mysql")
        self.ignoredPackages.append("libs/dbus")

        #self.defines["appname"] = "kirigamidemo"
        #self.defines["icon"] = os.path.join(self.sourceDir(), "data", "icons", "kdenlive.ico")
        self.defines["icon_png"] = self.sourceDir() / "android/res/drawable/logo.png"
        return super().createPackage()
