# -*- coding: utf-8 -*-
import info
from Package.QMakePackageBase import *


class subinfo(info.infoclass):
    def registerOptions(self):
        self.parent.package.categoryInfo.platforms = CraftCore.compiler.Platforms.NotAndroid

    def setDependencies(self):
        self.buildDependencies["libs/qt5/qtscript"] = None

    def setTargets(self):
        self.description = "Q Light Controller Plus (QLC+) is a free and cross-platform software to control DMX or analog lighting systems like moving heads, dimmers, scanners etc. This project is a fork of the great QLC project written by Heikki Junnila that aims to continue the QLC development and to introduce new features."

        # tarball targets (releases)
        for ver in ['4.12.6']:
            self.targets[ver] = f"https://github.com/mcallegari/qlcplus/archive/refs/tags/QLC+_{ver}.tar.gz"
            self.targetInstSrc[ver] = f"QLC+_{ver}"
            self.patchToApply[ver] += [("qlcplus-20221225.patch", 1)]
            self.patchLevel[ver] = 1
        self.targetDigests['4.12.6'] = (['e6b2b49a3a34b6a8240b451f2778f4752bb091c8376fc39c828ca294993fe377'], CraftHash.HashAlgorithm.SHA256)

        # git targets
        self.svnTargets['master'] = "https://github.com/mcallegari/qlcplus.git"
        self.patchToApply["master"] += [("qlcplus-20221225.patch", 1)]
        self.patchLevel["master"] = 1

        self.defaultTarget = '4.12.6'


class Package(QMakePackageBase):
    def __init__(self, **args):
        QMakePackageBase.__init__(self)
        self.subinfo.options.useShadowBuild = False
        #self.subinfo.options.configure.autoreconf = False

    def configure(self):
        #defines = f' "INSTALLROOT = {os.path.join(CraftStandardDirs.craftRoot())}"'
        defines = ""
        return QMakePackageBase.configure(self, defines)


    #def createPackage(self):
        #self.addExecutableFilter(r"bin/(?!(grep)).*")
        #self.blacklist_file.append(self.packageDir() / 'blacklist.txt')
        #return super().createPackage()

